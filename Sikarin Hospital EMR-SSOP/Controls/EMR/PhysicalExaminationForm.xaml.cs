﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Sikarin_Hospital_EMR_SSOP.Controls.EMR
{
    /// <summary>
    /// Interaction logic for PhysicalExaminationForm.xaml
    /// </summary>
    public partial class PhysicalExaminationForm : UserControl
    {
        public string GeneralAppearance { get; set; } = "";
        public string PatientShape { get; set; } = "";
        public string Pale { get; set; } = "";
        public string Icteric { get; set; } = "";
        public string Edema { get; set; } = "";
        public string HEENT { get; set; } = "";
        public string Thyroid { get; set; } = "";
        public string CardiovascularSystem { get; set; } = "";
        public string RespiratorySystem { get; set; } = "";
        public string Abdomen { get; set; } = "";
        public string Skin { get; set; } = "";
        public string Extremities { get; set; } = "";
        public string NervousSystem { get; set; } = "";
        public string GenitoUrinarySystem { get; set; } = "";
        public string PhysicalExam { get; set; } = "";
        public string LaboratoryInvestigation { get; set; } = "";
        public string RadiologyInvestigation { get; set; } = "";
        public string Diagnosis { get; set; } = "";
        public string TreatmentAndSurgicalPlan { get; set; } = "";
        public string FollowUpReason { get; set; } = "";
        public string FollowUpInstruction { get; set; } = "";
        public string ConditionInCaseAdminTransfer { get; set; } = "";

        public PhysicalExaminationForm()
        {
            InitializeComponent();
        }
    }
}
