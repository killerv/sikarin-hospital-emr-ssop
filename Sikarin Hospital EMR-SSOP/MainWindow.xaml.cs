﻿using Sikarin_Hospital_EMR_SSOP.Helper;
using Sikarin_Hospital_EMR_SSOP.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Sikarin_Hospital_EMR_SSOP
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        readonly System.Globalization.CultureInfo cultureinfo = new System.Globalization.CultureInfo("th-TH");

        public static MainWindow _instance;
        private static string programPath = AppDomain.CurrentDomain.BaseDirectory;

        public MainWindow()
        {
            _instance = this;
            DataContext = this;

            InitializeComponent();
        }

        private void Grid_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            EMRWindow eMRWindow = new EMRWindow();
            eMRWindow.Show();
            eMRWindow.Activate();
        }

        private void Grid_PreviewMouseDown_1(object sender, MouseButtonEventArgs e)
        {
            SSOPWindow sSOPWindow= new SSOPWindow();
            sSOPWindow.Show();
            sSOPWindow.Activate();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
        }
    }
}
