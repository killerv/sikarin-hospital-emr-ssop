﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Sikarin_Hospital_EMR_SSOP.Controls.EMR
{
    /// <summary>
    /// Interaction logic for PatientInfo.xaml
    /// </summary>
    public partial class PatientInfo : UserControl
    {

        readonly System.Globalization.CultureInfo cultureInfo = new System.Globalization.CultureInfo("th-TH");

        private string patientName = "";
        public string PatientName
        {
            get => patientName;
            set
            {
                patientName = value;
            }
        }

        private string age = "";
        public string Age
        {
            get => age;
            set
            {
                age = value;
            }
        }

        private string patientID = "";
        public string PatientID
        {
            get => patientID;
            set
            {
                patientID = value;
            }
        }

        private string dateOfBirth = "";
        public string DateOfBirth
        {
            get => dateOfBirth;
            set
            {
                dateOfBirth = value;
            }
        }

        private string encounterID = "";
        public string EncounterID
        {
            get => encounterID;
            set
            {
                encounterID = value;
            }
        }

        private string encounterType = "";
        public string EncounterType
        {
            get => encounterType;
            set
            {
                encounterType = value;
            }
        }

        private string admissionDate = "";
        public string AdmissionDate
        {
            get => admissionDate;
            set
            {
                admissionDate = value;
            }
        }

        private string specialty = "";
        public string Specialty
        {
            get => specialty;
            set
            {
                specialty = value;
            }
        }

        private string attendingPractitioner = "";
        public string AttendingPractitioner
        {
            get => attendingPractitioner;
            set
            {
                attendingPractitioner = value;
            }
        }

        private string location = "";
        public string Location
        {
            get => location;
            set
            {
                location = value;
            }
        }

        public PatientInfo()
        {
            InitializeComponent();

            this.DataContext = this;

            DateTime dateOfBirthDateTime = new DateTime(1991, 03, 29);
            DateTime admissionDateTime = new DateTime(2019, 08, 21, 17, 03, 00);

            PatientName = "นาย ฤชา วงศ์ธนากาญจน์";
            Age = "28Y";
            PatientID = "55335443";
            DateOfBirth = dateOfBirthDateTime.ToString("dd/MM/yyyy", cultureInfo);
            EncounterID = "179384610001";
            EncounterType = "ผู้ป่วยนอก";
            AdmissionDate = admissionDateTime.ToString("dd/MM/yyyy HH:mm", cultureInfo);
            Specialty = "Medicine";
            AttendingPractitioner = "นพ.อรุณ เลิศวรวิวัฒน์";
            Location = "อายุรกรรม 1";
        }
    }
}
