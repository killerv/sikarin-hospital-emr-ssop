﻿using Sikarin_Hospital_EMR_SSOP.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Sikarin_Hospital_EMR_SSOP.Controls.SSOP
{
    /// <summary>
    /// Interaction logic for SSOPForm.xaml
    /// </summary>
    public partial class SSOPForm : UserControl, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        private PatientEncounter patientEncounter = new PatientEncounter();
        public PatientEncounter PatientEncounter { 
            get => patientEncounter; 
            set
            {
                patientEncounter =
                    faxCliamThaiForm.PatientEncounter = 
                    faxCliamInterForm.PatientEncounter = value;

                OnPropertyChanged("PatientEncounter");
            }
        }

        public SSOPForm()
        {
            DataContext = this;
            InitializeComponent();
        }
    }
}
