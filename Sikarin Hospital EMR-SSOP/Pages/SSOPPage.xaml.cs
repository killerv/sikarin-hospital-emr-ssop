﻿using Sikarin_Hospital_EMR_SSOP.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Sikarin_Hospital_EMR_SSOP.Pages
{
    /// <summary>
    /// Interaction logic for SSOPPage.xaml
    /// </summary>
    public partial class SSOPPage : Page
    {
        public List<PatientEncounter> SearchResult = new List<PatientEncounter>();

        public PatientEncounter SelectedPatientEncounter = new PatientEncounter();

        public SSOPPage()
        {
            this.DataContext = this;

            InitializeComponent();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            //SearchResult.Add(new Patient
            //{
            //    Name = "นาย ฤชา วงศ์ธนากาญจน์",
            //    PatientID = "55335443",
            //    EncounterID = "179384610001",
            //    AdmissionDate = "21/08/2562 17:03"
            //});

            SearchResult = SSOPWindow._instance.PatientEncounters;
            listView.ItemsSource = SearchResult;
        }

        private void FormMenu_OnFormMenuPressed(object sender, Controls.Menu.FormMenuPressedEventArgs e)
        {
            switch (e.buttonPressed)
            {
                case "Print":
                    MessageBox.Show("Printing");
                    break;
                case "Save":
                    MessageBox.Show("Saved");
                    break;
                case "Close":
                    stackPanelForm.Visibility = Visibility.Hidden;
                    stackPanelList.Visibility = Visibility.Visible;
                    break;
            }
        }

        private void Image_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            stackPanelForm.Visibility = Visibility.Visible;
            stackPanelList.Visibility = Visibility.Hidden;
        }

        private void listView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (listView.SelectedValue != null)
            {
                ssopForm.PatientEncounter = (PatientEncounter) listView.SelectedValue;

                stackPanelForm.Visibility = Visibility.Visible;
                stackPanelList.Visibility = Visibility.Hidden;

                listView.SelectedIndex = -1;
            }
        }
    }
}
