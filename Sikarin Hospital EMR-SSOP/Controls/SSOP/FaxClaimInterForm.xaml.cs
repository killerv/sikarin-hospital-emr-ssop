﻿using Sikarin_Hospital_EMR_SSOP.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Sikarin_Hospital_EMR_SSOP.Controls.SSOP
{
    /// <summary>
    /// Interaction logic for FaxClaimInterForm.xaml
    /// </summary>
    public partial class FaxClaimInterForm : UserControl, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        private PatientEncounter patientEncounter = new PatientEncounter();
        public PatientEncounter PatientEncounter
        {
            get => patientEncounter;
            set
            {
                patientEncounter = value;
                OnPropertyChanged("PatientEncounter");
            }
        }

        private string investigation;
        public string Investigation
        {
            get => investigation;
            set
            {
                investigation = string.IsNullOrEmpty(value) ? "" : value.Replace("<br>", "\n");
                OnPropertyChanged("Investigation");
            }
        }

        public FaxClaimInterForm()
        {
            this.DataContext = this;
            InitializeComponent();
        }

        private void InvestigationType_Checked(object sender, RoutedEventArgs e)
        {
            if (PatientEncounter.DischargeSummary == null) return;

            RadioButton radioButton = (RadioButton)sender;
            switch (radioButton.Content.ToString())
            {
                case "Diagnotic/Investigation Lab":
                    Investigation = PatientEncounter.DischargeSummary.DS03_DIAGNOSTIC_INVESTIGATION_LAB;
                    break;
                case "Diagnotic/Investigation X-Rays":
                    Investigation = PatientEncounter.DischargeSummary.DS03_DIAGNOSTIC_INVESTIGATION_XRAY;
                    break;
                case "Pathology Report":
                    Investigation = PatientEncounter.DischargeSummary.DS03_PATHOLOGY_REPORT;
                    break;
            }
        }
    }
}
