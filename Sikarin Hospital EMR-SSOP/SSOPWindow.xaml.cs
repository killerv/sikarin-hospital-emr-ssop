﻿using Sikarin_Hospital_EMR_SSOP.Helper;
using Sikarin_Hospital_EMR_SSOP.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Sikarin_Hospital_EMR_SSOP
{
    /// <summary>
    /// Interaction logic for SSOPWindow.xaml
    /// </summary>
    public partial class SSOPWindow : Window
    {
        public static SSOPWindow _instance;
        private static string programPath = AppDomain.CurrentDomain.BaseDirectory;

        public List<PatientEncounter> PatientEncounters = new List<PatientEncounter>();

        public SSOPWindow()
        {
            _instance = this;
            DataContext = this;

            InitializeComponent();
        }


        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            XMLHelper xmlHelper = new XMLHelper();
            PatientEncounters = xmlHelper.ReadPatientDatabase(programPath + "database.xml");

            frame.Navigate(new Sikarin_Hospital_EMR_SSOP.Pages.SSOPPage());
        }
    }
}
