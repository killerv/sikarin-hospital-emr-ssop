﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Sikarin_Hospital_EMR_SSOP.Controls.EMR
{
    /// <summary>
    /// Interaction logic for PFEForm.xaml
    /// </summary>
    public partial class PFEForm : UserControl
    {
        public string PFE { get; set; } = "";
        public string BarriesOfDataReciever { get; set; } = "";
        public string ReadinessAndWillingness { get; set; } = "";
        public string TeachingMethod { get; set; } = "";

        public string EducationTo { get; set; } = "";
        public string EducationTopic { get; set; } = "";
        public string PFEOutcome { get; set; } = "";
        public string Physician { get; set; } = "";

        public PFEForm()
        {
            InitializeComponent();
            this.DataContext = this;
        }
    }
}
