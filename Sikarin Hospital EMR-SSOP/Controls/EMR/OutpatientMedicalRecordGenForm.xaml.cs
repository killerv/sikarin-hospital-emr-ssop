﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Sikarin_Hospital_EMR_SSOP.Controls.EMR
{
    /// <summary>
    /// Interaction logic for OutpatientMedicalRecordGenForm.xaml
    /// </summary>
    public partial class OutpatientMedicalRecordGenForm : UserControl, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        readonly System.Globalization.CultureInfo cultureInfo = new System.Globalization.CultureInfo("th-TH");

        private DateTime seeDocterDateTime = new DateTime();
        public DateTime SeeDocterDateTime
        {
            get => seeDocterDateTime;
            set
            {
                seeDocterDateTime = value;
            }
        }

        public string SeeDocterDateTimeString
        {
            get => seeDocterDateTime.ToString("dd/MM/yyyy HH:mm", cultureInfo);
            set
            {
                seeDocterDateTime = Convert.ToDateTime(value);
            }
        }

        private string height = "";
        public string PatientHeight
        {
            get => height;
            set
            {
                height = value;
                OnPropertyChanged("BMI");
            }
        }

        private string weight = "";
        public string Weight
        {
            get => weight;
            set
            {
                weight = value;
                OnPropertyChanged("BMI");
            }
        }

        public string BMI
        {
            get
            {
                double weight = 0;
                double height = 0;

                double.TryParse(Weight, out weight);
                double.TryParse(PatientHeight, out height);
                
                if (height > 0)
                {
                    return Math.Round((weight / Math.Pow(height / 100, 2)), 2).ToString();
                }

                return "0";
            }
        }

        private string temperature = "";
        public string Temperature
        {
            get => temperature;
            set
            {
                temperature = value;
            }
        }

        private string pulse = "";
        public string Pulse
        {
            get => pulse;
            set
            {
                pulse = value;
            }
        }

        private string respiratoryRate = "";
        public string RespiratoryRate
        {
            get => respiratoryRate;
            set
            {
                respiratoryRate = value;
            }
        }

        private string bloodPressure = "";
        public string BloodPressure
        {
            get => bloodPressure;
            set
            {
                bloodPressure = value;
            }
        }

        private string repeatBloodPressure = "";
        public string RepeatBloodPressure
        {
            get => repeatBloodPressure;
            set
            {
                repeatBloodPressure = value;
            }
        }

        private string spO2 = "";
        public string SpO2
        {
            get => spO2;
            set
            {
                spO2 = value;
            }
        }

        private string dataGiver = "ผู้ป่วย";
        public string DataGiver
        {
            get => dataGiver;
            set
            {
                dataGiver = value;
            }
        }

        public bool IsAlcohol { get; set; } = false;

        private string alcohol = "";
        public string Alcohol
        {
            get => alcohol;
            set
            {
                alcohol = value;
            }
        }

        public bool IsSmoking { get; set; } = false;

        private string smoking = "";
        public string Smoking
        {
            get => smoking;
            set
            {
                smoking = value;
            }
        }

        private string allergy = "";
        public string Allergy
        {
            get => allergy;
            set
            {
                allergy = value;
            }
        }

        private string externalDrugUse = "";
        public string ExternalDrugUse
        {
            get => externalDrugUse;
            set
            {
                externalDrugUse = value;
            }
        }

        private string chiefComplaint = "";
        public string ChiefComplaint
        {
            get => chiefComplaint;
            set
            {
                chiefComplaint = value;
            }
        }

        private string presentIllnessAndPreviousTreatment = "";
        public string PresentIllnessAndPreviousTreatment
        {
            get => presentIllnessAndPreviousTreatment;
            set
            {
                presentIllnessAndPreviousTreatment = value;
            }
        }

        private string pastHistory = "";
        public string PastHistory
        {
            get => pastHistory;
            set
            {
                pastHistory = value;
            }
        }

        public OutpatientMedicalRecordGenForm()
        {
            InitializeComponent();

            this.DataContext = this;
            SeeDocterDateTime = new DateTime(2019, 08, 21, 17, 22, 00);
        }
    }
}
