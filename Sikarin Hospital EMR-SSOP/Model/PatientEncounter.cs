﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sikarin_Hospital_EMR_SSOP.Model
{
    public class PatientEncounter
    {
        public string PatientName { get; set; }
        public string PatientID { get; set; }
        public string AdmissionDate { get; set; }
        public string AttendingPractitioner { get; set; } //Clinical note Attending
        public string Age { get; set; }
        public string DateOfBirth { get; set; }
        public string Specialty { get; set; } //Clinical note Specialty
        public string Location { get; set; }
        public string PatientClass { get; set; }
        public string EncounterType { get; set; }
        public string EncounterDate { get; set; }
        public string EncounterID { get; set; }
        public string ServiceCode { get; set; }
        public string ShortDesc { get; set; }
        public string LongDesc { get; set; }

        //DS03 - Discharge Summary
        public DischargeSummary DischargeSummary { get; set; } = new DischargeSummary();
        public string DischargeSummaryHTML { get; set; }

        //IP14 - Admission Note
        public AdmissionNote AdmissionNote { get; set; } = new AdmissionNote();
        public string AdmissionNoteHTML { get; set; }
    }
}
