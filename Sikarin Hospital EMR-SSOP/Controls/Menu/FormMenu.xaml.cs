﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Sikarin_Hospital_EMR_SSOP.Controls.Menu
{
    public class FormMenuPressedEventArgs : EventArgs
    {
        public string buttonPressed;

        public FormMenuPressedEventArgs(string name = null)
        {
            buttonPressed = name;
        }

    }

    /// <summary>
    /// Interaction logic for FormMenu.xaml
    /// </summary>
    public partial class FormMenu : UserControl
    {
        public event EventHandler<FormMenuPressedEventArgs> OnFormMenuPressed;

        public FormMenu()
        {
            InitializeComponent();
        }

        private void Button_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            Image buttonImage = (Image)sender;

            OnFormMenuPressed?.Invoke(this, new FormMenuPressedEventArgs(buttonImage.Name));
        }
    }
}
