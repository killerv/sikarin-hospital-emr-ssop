﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sikarin_Hospital_EMR_SSOP.Model
{
    public class AdmissionNote
    {
        public string IP14_TEMPERATURE { get; set; }
        public string IP14_BLOOD_PRESSURE { get; set; }
        public string IP14_PULSE { get; set; }
        public string IP14_RESPIRATORY_RATE { get; set; }
        public string IP14_CHIEF_COMPLAINT { get; set; }
        public string IP14_PRESENT_ILLNESS_AND_PREVIOUS_TREATMENT { get; set; }
        public string IP14_PAST_HISTORY { get; set; }
        public string IP14_DIAGNOSIS { get; set; }
        public string IP14_REASON_FOR_ADMISSION { get; set; }
        public string IP14_TREATMENT_AND_SURGICAL_PLAN { get; set; }
        public string IP14_EXPECTED_LENGTH_OF_DAY { get; set; }
        public string IP14_PHYSICAL_EXAM { get; set; }

        public string IP14_ADDED_BY_ID { get; set; }
        public string IP14_ADDED_DATE { get; set; }
    }
}
