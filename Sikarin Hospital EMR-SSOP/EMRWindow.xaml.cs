﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Sikarin_Hospital_EMR_SSOP
{
    /// <summary>
    /// Interaction logic for EMRWindow.xaml
    /// </summary>
    public partial class EMRWindow : Window
    {
        public EMRWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            frame.Navigate(new Sikarin_Hospital_EMR_SSOP.Pages.EMRPage());
        }
    }
}
