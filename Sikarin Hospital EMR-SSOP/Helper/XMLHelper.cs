﻿using Sikarin_Hospital_EMR_SSOP.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;

namespace Sikarin_Hospital_EMR_SSOP.Helper
{
    class XMLHelper
    {
        protected static string contentFolder = AppDomain.CurrentDomain.BaseDirectory + "content";

        public XMLHelper()
        {
            if (!Directory.Exists(contentFolder))
            {
                Directory.CreateDirectory(contentFolder);
            }
        }

        public List<PatientEncounter> ReadPatientDatabase(string filepath)
        {
            if (!File.Exists(filepath))
            {
                throw new FileNotFoundException();
            }

            XmlTextReader xmlReader = new XmlTextReader(filepath);

            string currentDataColumn = "";
            string currentNoteType = "";
            string currentNoteTypeDesc = "";
            string addedByID = "";
            string addedDate = "";

            List<PatientEncounter> patientEncounters = new List<PatientEncounter>();
            PatientEncounter patientEncounter = new PatientEncounter();
            while (xmlReader.Read())
            {
                switch (xmlReader.NodeType)
                {
                    case XmlNodeType.Element:
                        currentDataColumn = "";
                        if (xmlReader.Name == "RESULTS") // Depth = 0
                        {
                            continue;
                        }
                        else if (xmlReader.Name == "ROW") // Depth = 1
                        {
                            if (IsValidPatientEncounter(patientEncounter))
                            {
                                patientEncounters = AddOrUpdatePatientEncounterList(patientEncounters, patientEncounter, currentNoteType);
                            }

                            patientEncounter = new PatientEncounter();
                            currentNoteType = "";
                            currentNoteTypeDesc = "";
                            addedByID = "";
                            addedDate = "";
                        }
                        else if (xmlReader.Name == "COLUMN") // Depth = 2
                        {
                            currentDataColumn = xmlReader.GetAttribute("NAME");
                        }
                        break;
                    case XmlNodeType.CDATA:
                        switch (currentDataColumn)
                        {
                            case "PATIENT_NAME":
                                patientEncounter.PatientName = xmlReader.Value;
                                break;
                            case "PATIENT_ID":
                                patientEncounter.PatientID = xmlReader.Value;
                                break;
                            case "ADMISSION_DATE":
                                patientEncounter.AdmissionDate = xmlReader.Value;
                                break;
                            case "ATTENDING_PRACTITIONER":
                                patientEncounter.AttendingPractitioner = xmlReader.Value;
                                break;
                            case "AGE":
                                patientEncounter.Age = xmlReader.Value;
                                break;
                            case "DATE_OF_BIRTH":
                                patientEncounter.DateOfBirth = xmlReader.Value;
                                break;
                            case "SPECIALTY":
                                patientEncounter.Specialty = xmlReader.Value;
                                break;
                            case "LOCATION":
                                patientEncounter.Location = xmlReader.Value;
                                break;
                            case "ENCOUNTER_TYPE":
                                patientEncounter.EncounterType = xmlReader.Value;
                                break;
                            case "ENCNTR_DATE":
                                patientEncounter.EncounterDate = xmlReader.Value;
                                break;
                            case "ENCOUNTER_ID":
                                patientEncounter.EncounterID = xmlReader.Value;
                                break;
                            case "PATIENT_CLASS":
                                patientEncounter.PatientClass = xmlReader.Value;
                                break;
                            case "NOTE_TYPE":
                                currentNoteType = xmlReader.Value;
                                break;
                            case "NOTE_TYPE_DESC":
                                currentNoteTypeDesc = xmlReader.Value;
                                break;
                            case "SERVICE_CODE":
                                patientEncounter.ServiceCode = xmlReader.Value;
                                break;
                            case "SHORT_DESC":
                                patientEncounter.ShortDesc = xmlReader.Value;
                                break;
                            case "LONG_DESC":
                                patientEncounter.LongDesc = xmlReader.Value;
                                break;
                            case "ADDED_BY_ID":
                                addedByID = xmlReader.Value;
                                break;
                            case "ADDED_DATE":
                                addedDate = xmlReader.Value;
                                break;
                            case "NOTE_CONTENT":
                                if (new string[] { "IP14", "DS03" }.Where(s => currentNoteType == s).Count() > 0)
                                {
                                    string noteContentXML = ProcessNoteContent(xmlReader.Value, patientEncounter.PatientID, currentNoteType, currentNoteTypeDesc);

                                    if (currentNoteType == "DS03")
                                    {
                                        patientEncounter.DischargeSummary = TranslateDischargeSummary(noteContentXML);
                                        patientEncounter.DischargeSummary.DS03_ADDED_BY_ID = addedByID;
                                        patientEncounter.DischargeSummary.DS03_ADDED_DATE = addedDate;
                                        patientEncounter.DischargeSummaryHTML = xmlReader.Value;
                                    }
                                    else if (currentNoteType == "IP14")
                                    {
                                        patientEncounter.AdmissionNote = TranslateAdmissionNote(noteContentXML);
                                        patientEncounter.AdmissionNote.IP14_ADDED_BY_ID = addedByID;
                                        patientEncounter.AdmissionNote.IP14_ADDED_DATE = addedDate;
                                        patientEncounter.AdmissionNoteHTML = xmlReader.Value;
                                    }
                                }
                                break;
                        }
                        break;
                }
            }

            if (IsValidPatientEncounter(patientEncounter))
            {
                patientEncounters = AddOrUpdatePatientEncounterList(patientEncounters, patientEncounter, currentNoteType);
            }

            return patientEncounters;
        }

        private List<PatientEncounter> AddOrUpdatePatientEncounterList(List<PatientEncounter> patientEncounters, PatientEncounter patientEncounter, string noteType)
        {

            int index = patientEncounters.FindIndex(pe => pe.EncounterID == patientEncounter.EncounterID);
            if (index < 0)
            {
                patientEncounters.Add(patientEncounter);
            }
            else
            {
                switch (noteType)
                {
                    case "DS03":
                        patientEncounters[index].DischargeSummary = patientEncounter.DischargeSummary;
                        break;
                    case "IP14":
                        patientEncounters[index].AdmissionNote = patientEncounter.AdmissionNote;
                        break;
                }

            }

            return patientEncounters;
        }

        private string ProcessNoteContent(string html, string parentID, string noteType, string noteTypeDesc)
        {
            html = System.Net.WebUtility.HtmlDecode(html);
            html = Regex.Replace(html, @"[\r\n\t]|\s{4,}|<font[^<]*>\s*:?\s*<\/font>", "", RegexOptions.IgnoreCase); //Remove new line, tab, <font>:</font>, <font></font>
            html = Regex.Replace(html, @">\s*([^<]*)\s*<", ">$1<");
            html = Regex.Replace(html, @"<td[^<]*>\s*<\/td>", "", RegexOptions.IgnoreCase);
            html = Regex.Replace(html, @"\s*(<\/?font>)\s*", "$1", RegexOptions.IgnoreCase);
            html = Regex.Replace(html, @"<\/font><font[^<]*>", "", RegexOptions.IgnoreCase);
            html = Regex.Replace(html, @"(<tr[^<]*>)", "\n$1", RegexOptions.IgnoreCase);
            html = Regex.Replace(html, @"(<\/tr>)(?!<tr>)([^\n]+)", "$1\n$2", RegexOptions.IgnoreCase);
            html = Regex.Replace(html, @"((<(font)[^<]*>[^<]*)\s*)<font[^<]*>([^<]*)<\/font>(<\/font>)", "$1$4$5", RegexOptions.IgnoreCase);

            html = Regex.Replace(
                html,
                @"<tr[^>]*><td[^>]*><font[^>]*>(?!<\/font>)(.*)\s*<\/font>(<\/td>)?<td[^>]*><font[^>]*>(?!<\/font>)(.*)\s*<\/font>(.*)\s*<\/td><\/tr>",
                "<COLUMN NAME=\"$1\"><![CDATA[$3]]></COLUMN>",
                RegexOptions.IgnoreCase
                ); //Content

            html = Regex.Replace(
                html,
                @"<tr[^>]*><td[^>]*><font[^>]*><[ui]>(?!<\/font>)(.*)\s*<\/[ui]><\/font><\/td><\/tr>",
                "<COLUMN NAME=\"Header\"><![CDATA[$1]]></COLUMN>",
                RegexOptions.IgnoreCase
                ); //Header

            html = html.Replace("&", ";amp;");

            html = Regex.Replace(html, @"<\/?(html|table|tbody|tr|td).*\n?", "", RegexOptions.IgnoreCase); //Remove unuse line
            html = $"<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n<result>\n{html}</result>";

            string filename = new string(
                $"{string.Join("_", new string[] { parentID, noteType, noteTypeDesc }) }"
                .Where(ch => !Path.GetInvalidFileNameChars().Contains(ch)).ToArray());

            File.WriteAllText($"{contentFolder}/{filename}.xml", html, Encoding.Unicode);

            return html;
        }

        public string FormatValue(string value)
        {
            return value.Replace(";amp;", "&").Trim();
        }

        private DischargeSummary TranslateDischargeSummary(string xmlText)
        {
            DischargeSummary ds = new DischargeSummary();
            XmlTextReader xmlReader = new XmlTextReader(new StringReader(xmlText));
            string currentDataColumn = "";

            while (xmlReader.Read())
            {
                switch (xmlReader.NodeType)
                {
                    case XmlNodeType.Element:
                        currentDataColumn = "";
                        if (xmlReader.Name == "COLUMN")
                        {
                            currentDataColumn = xmlReader.GetAttribute("NAME");
                        }
                        break;
                    case XmlNodeType.CDATA:
                        string value = FormatValue(xmlReader.Value);
                        switch (currentDataColumn)
                        {
                            case "Medical License No":
                                ds.DS03_MEDICAL_LICENSE_NO = value;
                                break;
                            case "Diagnostic/investigation (LAB) ที่สำคัญ":
                                ds.DS03_DIAGNOSTIC_INVESTIGATION_LAB = value;
                                break;
                            case "Diagnostic/Investigation (X-RAY) ที่สำคัญ":
                                ds.DS03_DIAGNOSTIC_INVESTIGATION_XRAY = value;
                                break;
                            case "Pathology Report":
                                ds.DS03_PATHOLOGY_REPORT = value;
                                break;
                            case "Principle Diagnosis/ICD10":
                                ds.DS03_PRINCIPLE_DIAGNOSIS_ICD10 = value;
                                break;
                            case "Co-morbidities/ICD10":
                                ds.DS03_CO_MORBIDITIES_ICD10 = value;
                                break;
                            case "Complications/ICD10":
                                ds.DS03_COMPLICATIONS_ICD10 = value;
                                break;
                            case "Other Diagnosis/ICD10":
                                ds.DS03_OTHER_DIAGNOSIS_ICD10 = value;
                                break;
                            case "Procedure/Operation(DATE -Time in- Time out)/ICD9":
                                ds.DS03_COMPLICATIONS_ICD10 = value;
                                break;
                            case "Patient condition upon discharge":
                                ds.DS03_PATIENT_CONDITION_UPON_DISCHARGE = value;
                                break;
                        }
                        break;
                }

            }

            return ds;
        }

        private AdmissionNote TranslateAdmissionNote(string xmlText)
        {
            AdmissionNote admissionNote = new AdmissionNote();
            XmlTextReader xmlReader = new XmlTextReader(new StringReader(xmlText));
            string currentDataColumn = "";

            while (xmlReader.Read())
            {
                switch (xmlReader.NodeType)
                {
                    case XmlNodeType.Element:
                        currentDataColumn = "";
                        if (xmlReader.Name == "COLUMN")
                        {
                            currentDataColumn = xmlReader.GetAttribute("NAME");
                        }
                        break;
                    case XmlNodeType.CDATA:
                        string value = FormatValue(xmlReader.Value);
                        switch (currentDataColumn)
                        {
                            case "Temperature (องศาเซลเซียส)":
                                admissionNote.IP14_TEMPERATURE = value;
                                break;
                            case "Blood Pressure (mmHg)":
                                admissionNote.IP14_BLOOD_PRESSURE = value;
                                break;
                            case "Pulse(bpm)":
                                admissionNote.IP14_PULSE = value;
                                break;
                            case "Respiratory rate (bpm)":
                                admissionNote.IP14_RESPIRATORY_RATE = value;
                                break;
                            case "Chief Complaint":
                                admissionNote.IP14_CHIEF_COMPLAINT = value;
                                break;
                            case "Present IIIness ;amp; Previous Treatment":
                                admissionNote.IP14_PRESENT_ILLNESS_AND_PREVIOUS_TREATMENT = value;
                                break;
                            case "Past History / Underlying Diseases / Family History":
                                admissionNote.IP14_PAST_HISTORY = value;
                                break;
                            case "Diagnosis":
                                admissionNote.IP14_DIAGNOSIS = value;
                                break;
                            case "Reason For Admission":
                                admissionNote.IP14_REASON_FOR_ADMISSION = value;
                                break;
                            case "Treatment and Surgical Plan":
                                admissionNote.IP14_TREATMENT_AND_SURGICAL_PLAN = value;
                                break;
                            case "ระยะเวลาที่คาดว่าจะอยู่ร.พ.โดยประมาณ (วัน)":
                                admissionNote.IP14_EXPECTED_LENGTH_OF_DAY = value;
                                break;
                            case "Physical exam/ General Condition Signification Findings":
                                admissionNote.IP14_PHYSICAL_EXAM = value;
                                break;
                        }
                        break;
                }

            }

            return admissionNote;
        }

        public bool IsValidPatientEncounter(PatientEncounter patientEncounter)
        {
            return !(string.IsNullOrEmpty(patientEncounter.PatientID) || string.IsNullOrEmpty(patientEncounter.EncounterID));
        }
    }
}
