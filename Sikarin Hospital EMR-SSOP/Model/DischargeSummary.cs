﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sikarin_Hospital_EMR_SSOP.Model
{
    public class DischargeSummary
    {
        public string DS03_MEDICAL_LICENSE_NO { get; set; }
        public string DS03_DIAGNOSTIC_INVESTIGATION_LAB { get; set; }
        public string DS03_DIAGNOSTIC_INVESTIGATION_XRAY { get; set; }
        public string DS03_PATHOLOGY_REPORT { get; set; }
        public string DS03_PRINCIPLE_DIAGNOSIS_ICD10 { get; set; }
        public string DS03_CO_MORBIDITIES_ICD10 { get; set; }
        public string DS03_COMPLICATIONS_ICD10 { get; set; }
        public string DS03_OTHER_DIAGNOSIS_ICD10 { get; set; }
        public string DS03_PROCEDURE_OPERATION_DATE_TIME_IN_TIME_OUT_ICD9 { get; set; }
        public string DS03_PATIENT_CONDITION_UPON_DISCHARGE { get; set; }
        public string DS03_ADDED_BY_ID { get; set; }
        public string DS03_ADDED_DATE { get; set; }
    }
}
